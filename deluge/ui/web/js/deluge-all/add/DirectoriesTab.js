/**
 * Deluge.add.DirectoriesTab.js
 *
 * This file is licensed under GNU General Public License 3.0, or later, with
 * the additional special exception to link portions of this program with the OpenSSL library.
 * See LICENSE for more details.
 */
Ext.ns('Deluge.add');

/**
 * @class Deluge.add.DirectoriesTab
 * @extends Ext.Panel
*/
Deluge.add.DirectoriesTab = Ext.extend(Ext.Panel, {
    title: _('Directories'),
    border: false,
    bodyStyle: 'padding: 5px',
    disabled: true,
    labelWidth: 1,
    layout: 'anchor',

    _get_path: function () {
        return this.path_to_scan_field.input.getValue()
    },

    _set_path: function(path) {
        this.path_to_scan_field.input.setValue(path)
    },

    _fetchDirs: function (setDirs) {
        deluge.client.web.list_dirs(
            this._get_path(),
            {
                success: setDirs,
                failure: function (error) {
                    console.log(`Error listing dirs`, error);
                    setDirs([])
                }
            }
        )
    },

    _setDirs: function (dirs) {
        this.tree.setRootNode(
            {
                text: 'Root',
                nodeType: 'async',
                draggable: false,
                children: dirs,
                id: this._get_path(),
                loader: this.loader
            }
        );
    },

    _updateTree: function() {
        this._fetchDirs(this._setDirs.bind(this));
    },

    loader: new Ext.tree.TreeLoader({
        directFn: function (path, handleResponse) {
            deluge.client.web.list_dirs(
                path,
                {
                    success: (dirs) => {
                        handleResponse(
                            dirs,
                            {status: true}
                        );
                    },
                    failure: function (error) {
                        console.log(`Error expanding node`, error);
                    }
                }
            );
        }
    }),

    initComponent: function () {
        Deluge.add.DirectoriesTab.superclass.initComponent.call(this);

        //to be set by parent
        this.downloadLocationOptionsManager = null;

        this.path_to_scan_field = this.add(new Ext.ux.form.ButtonField({
            anchor: '100%'
        }))

        this.path_to_scan_field.input.setValue("/")
        this.path_to_scan_field.input.on('specialkey', function(field, e) {
            if (e.getKey() == e.ENTER) {
                this._updateTree();
            }
        }.bind(this));
        this.path_to_scan_field.button.setHandler(this._updateTree.bind(this))

        this.tree = this.add(
            new Ext.tree.TreePanel({
                height: 250,
                autoScroll: true,
                animate: true,
                containerScroll: true,
                border: false,
                style: 'padding-top: 5px',

                rootVisible: false,
                root: {
                    text: 'Root-original',
                    id: "/",
                    nodeType: 'async',
                    draggable: false
                },
                listeners: {
                    click: (n) => {
                        this.downloadLocationOptionsManager.update('download_location', n.attributes.id);
                        this._set_path(n.attributes.id);
                    },
                    afterrender: () => {
                        this._updateTree();
                    }
                }
            })
        );
    }
});
