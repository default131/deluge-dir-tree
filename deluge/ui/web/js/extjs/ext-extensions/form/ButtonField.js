/**
 * Ext.ux.form.ButtonField.js
 *
 * Copyright (c) Damien Churchill 2009-2010 <damoxc@gmail.com>
 *
 * This file is part of Deluge and is licensed under GNU General Public License 3.0, or later, with
 * the additional special exception to link portions of this program with the OpenSSL library.
 * See LICENSE for more details.
 */
Ext.namespace('Ext.ux.form');

/**
 * Ext.ux.form.ButtonField class
 *
 * @version v0.1
 *
 * @class Ext.ux.form.ButtonField
 * @extends Ext.form.TriggerField
 */
Ext.ux.form.ButtonField = Ext.extend(Ext.form.Field, {
    cls: 'x-button-field',

    initComponent: function () {
        Ext.ux.form.ButtonField.superclass.initComponent.call(this);

        this.button = new Ext.Button({
            text: 'Get tree'
        });

        this.input = new Ext.form.TextField({
            disabled: false
        });
    },

    onRender: function (ct, position) {
        if (!this.el) {
            this.panel = new Ext.Panel({
                cls: this.groupCls,
                layout: 'table',
                layoutConfig: {
                    columns: 2,
                },
                border: false,
                renderTo: ct,
            });
            this.panel.ownerCt = this;
            this.el = this.panel.getEl();

            this.panel.add(this.button);
            this.panel.add(this.input);
            this.panel.doLayout();

            this.button.getEl().parent().setStyle('padding-right', '10px');
        }
        Ext.ux.form.ButtonField.superclass.onRender.call(this, ct, position);
    },

    // private
    onResize: function (w, h) {
        this.panel.setSize(w, h);
        this.panel.doLayout();

        // we substract 10 for the padding :-)
        var inputWidth = w - this.button.getSize().width - 25;
        this.input.setSize(inputWidth, h);
    },

});
Ext.reg('buttonfield', Ext.ux.form.ButtonField);
