#!/usr/bin/env bash

#timezone
ln -sf /usr/share/zoneinfo/${TZ:-"Etc/UTC"} /etc/localtime

#user/group
UNAME=containeruser
GNAME=containeruser

userdel $UNAME
groupdel $GNAME

if [[ -n "$PUID" ]]; then
  useradd -mu ${PUID} $UNAME
else
  UNAME=root
fi

if [[ -n "$PGID" ]]; then
  groupadd -g ${PGID} $GNAME
else
  GNAME=root
fi

prepend() {
  while read line; do
    echo "$1 $line";
  done
}

#config dir
if su -g $GNAME $UNAME -c "test ! -w /config"; then
  chown $UNAME:$GNAME /config
fi

set -o pipefail

(deluged -dL ${DELUGE_LOGLEVEL:-info} -U $UNAME -g $GNAME -c /config | prepend 'DAEMON') &
(deluge-web -dL ${DELUGE_LOGLEVEL:-info} -U $UNAME -g $GNAME -c /config | prepend '   WEB') &

wait -n

exit $?