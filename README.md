# Deluge BitTorrent Client patch/fork

Adds a tab with a dynamically loaded filesystem tree, to choose download locations from. One no longer needs to type the path from memory.

![demo.gif](readme/demo.gif)

The JSON-RPC method for scanning the filesystem is behind the default `AUTH_LEVEL_NORMAL`. Only the immediate child directories of the one being scanned are sent to the frontend.

# Rudimentary image build
Don't know how to build Deluge for Alpine so the *linuxserver s6* base image isn't used. To build the image(`docker-compose.yaml` and/or `Dockerfile`) one needs these files:
- `deluge.recipe`
- `wrapper.sh`
