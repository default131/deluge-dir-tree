FROM debian:bookworm-slim as build

RUN apt update && apt install -y \
  #BUILD
  git \
  intltool \
  closure-compiler \
  #BUILD
  git-build-recipe \
  dh-python \
  python3-setuptools \
  python3-pip \
  dpkg-dev \
  python3-all \
&& apt clean

WORKDIR /bulid-dir/deluge-built

WORKDIR /bulid-dir/deluge-sources

#copy all to trigger rebuilds on source changes
COPY ./deluge.recipe ./

RUN git-build-recipe --allow-fallback-to-native deluge.recipe lp_build

RUN tar -xf lp_build/*.tar.xz --strip-components=1 -C ../deluge-built

WORKDIR /bulid-dir/deluge-built

RUN dpkg-buildpackage -b

FROM debian:bookworm-slim

WORKDIR /deluge

COPY --from=build /bulid-dir/*.deb ./

RUN apt update && apt install -y \
  #RUNTIME: WEB, COMMON
  python3 \
  python3-pyasn1 \
  python3-rencode \
  python3-twisted \
  python3-xdg \
  python3-chardet \
  python3-mako \
  python3-openssl \
  #RUNTIME: DAEMON
  python3-libtorrent \
  #RUNTIME: OWN
  ./deluge-common*.deb \
  ./deluge-web*.deb \
  ./deluged*.deb \
  acl \
&& apt clean

RUN rm ./deluge*

COPY ./wrapper.sh ./

ENTRYPOINT ["./wrapper.sh", "2>&1"]